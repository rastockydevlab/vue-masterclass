import { defineStore } from 'pinia'

import getJobs from '@/api/getJobs'

import { useUserStore } from '@/stores/user'
import type { Job } from '@/api/types'

export const FETCH_JOBS = 'FETCH_JOBS'
export const UNIQUE_ORGANIZATIONS = 'UNIQUE_ORGANIZATIONS'
export const UNIQUE_JOB_TYPES = 'UNIQUE_JOB_TYPES'
export const FILTERED_JOBS = 'FILTERED_JOBS'

export const INCLUDE_JOB_BY_ORGANIZATION = 'INCLUDE_JOB_BY_ORGANIZATION'
export const INCLUDE_JOB_BY_JOB_TYPE = 'INCLUDE_JOB_BY_JOB_TYPE'

export interface JobsState {
  jobs: Job[]
}

export const useJobsStore = defineStore('jobs', {
  state: (): JobsState => ({
    jobs: [],
  }),

  actions: {
    async [FETCH_JOBS]() {
      this.jobs = await getJobs()
    },
  },

  getters: {
    [UNIQUE_ORGANIZATIONS](state) {
      const orgs = new Set<String>()

      state.jobs.forEach((job) => {
        orgs.add(job.organization)
      })

      return orgs
    },

    [UNIQUE_JOB_TYPES](state) {
      const jobTypes = new Set<String>()

      state.jobs.forEach((job) => {
        jobTypes.add(job.jobType)
      })

      return jobTypes
    },

    [INCLUDE_JOB_BY_ORGANIZATION]: () => (job: Job) => {
      const userStore = useUserStore()

      const noSelectedOrganizations = userStore.selectedOrganizations.length === 0
      if (noSelectedOrganizations) return true

      return userStore.selectedOrganizations.includes(job.organization)
    },

    [INCLUDE_JOB_BY_JOB_TYPE]: () => (job: Job) => {
      const userStore = useUserStore()

      const noSelectedJobTypes = userStore.selectedJobTypes.length === 0
      if (noSelectedJobTypes) return true

      return userStore.selectedJobTypes.includes(job.jobType)
    },

    [FILTERED_JOBS](state): Job[] {
      return state.jobs
        .filter((job) => this.INCLUDE_JOB_BY_ORGANIZATION(job))
        .filter((job) => this.INCLUDE_JOB_BY_JOB_TYPE(job))
    },
  },
})
