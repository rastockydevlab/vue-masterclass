import { ref } from 'vue'

import usePreviousAndNextPages from '@/composables/usePreviousAndNextPages'

describe('usePreviousAndNextPages', () => {
  it('calculates page before the current one', () => {
    const currentPage = ref(2)
    const maxPage = ref(3)

    const { previousPage } = usePreviousAndNextPages(currentPage, maxPage)

    expect(previousPage.value).toBe(1)
  })

  describe('when current page is the first one', () => {
    it('does not calculate previous page', () => {
      const currentPage = ref(1)
      const maxPage = ref(3)

      const { previousPage } = usePreviousAndNextPages(currentPage, maxPage)

      expect(previousPage.value).toBeUndefined()
    })
  })

  it('calculates page after the current one', () => {
    const currentPage = ref(2)
    const maxPage = ref(3)

    const { nextPage } = usePreviousAndNextPages(currentPage, maxPage)

    expect(nextPage.value).toBe(3)
  })

  describe('when current page is the last one', () => {
    it('does not calculate next page', () => {
      const currentPage = ref(3)
      const maxPage = ref(3)

      const { nextPage } = usePreviousAndNextPages(currentPage, maxPage)

      expect(nextPage.value).toBeUndefined()
    })
  })
})
