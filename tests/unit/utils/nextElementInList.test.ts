import nextElementInList from '@/utils/nextElementInList'

describe('nextElementInList', () => {
  it('locates element in list and returns the next one', () => {
    const list = ['a', 'b', 'c', 'd']
    const value = 'c'

    const result = nextElementInList(list, value)

    expect(result).toBe('d')
  })

  it('locates next element when current is at the end of the list', () => {
    const list = ['a', 'b', 'c', 'd', 'e']
    const value = 'e'

    const result = nextElementInList(list, value)

    expect(result).toBe('a')
  })
})
