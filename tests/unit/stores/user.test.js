import { createPinia, setActivePinia } from 'pinia'

import { useUserStore } from '@/stores/user'

beforeEach(() => {
  setActivePinia(createPinia())
})

describe('state', () => {
  it('keeps track of whether the user is logged in', () => {
    const store = useUserStore()
    expect(store.isLoggedIn).toBe(false)
  })

  it('stores organizations that the user wants to filter by', () => {
    const store = useUserStore()
    expect(store.selectedOrganizations).toEqual([])
  })
})

describe('actions', () => {
  describe('login', () => {
    it('logs the user in', () => {
      const store = useUserStore()
      store.login()
      expect(store.isLoggedIn).toBe(true)
    })
  })

  describe('logout', () => {
    it('logs the user out', () => {
      const store = useUserStore()
      store.login()
      store.logout()
      expect(store.isLoggedIn).toBe(false)
    })
  })

  describe('ADD_SELECTED_ORGANIZATIONS', () => {
    it('updates organizations the user has chosen to filter jobs by', () => {
      const store = useUserStore()

      store.ADD_SELECTED_ORGANIZATIONS(['Org1', 'Org2'])

      expect(store.selectedOrganizations).toEqual(['Org1', 'Org2'])
    })
  })
})
