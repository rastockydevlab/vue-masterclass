import type { Mock } from 'vitest'
import { createPinia, setActivePinia } from 'pinia'

import axios from 'axios'
vi.mock('axios')
const axiosGetMock = axios.get as Mock

import type { Job } from '@/api/types'

import { useJobsStore } from '@/stores/jobs'
import { useUserStore } from '@/stores/user'

beforeEach(() => {
  setActivePinia(createPinia())
})

describe('state', () => {
  it('stores job listings', () => {
    const store = useJobsStore()
    expect(store.jobs).toEqual([])
  })
})

describe('actions', () => {
  describe('FETCH_JOBS', () => {
    it('makes API request and stores received jobs', async () => {
      axiosGetMock.mockResolvedValue({
        data: ['Job 1', 'Job 2'],
      })

      const store = useJobsStore()
      await store.FETCH_JOBS()

      expect(store.jobs).toEqual(['Job 1', 'Job 2'])
    })
  })
})

describe('getters', () => {
  const createJob = (job: Partial<Job> = {}): Job => ({
    id: 1,
    title: 'Title',
    organization: 'Organization',
    degree: 'Degree',
    jobType: 'Job Type',
    locations: ['Location 1', 'Location 2'],
    minimumQualifications: ['Minimum Qualification 1', 'Minimum Qualification 2'],
    preferredQualifications: ['Preferred Qualification 1', 'Preferred Qualification 2'],
    description: 'Description',
    dateAdded: 'Date Added',
    ...job,
  })

  describe('UNIQUE_ORGANIZATIONS', () => {
    it('finds unique organizations from list of jobs', () => {
      const store = useJobsStore()
      store.jobs = [
        createJob({ organization: 'Amazon' }),
        createJob({ organization: 'Google' }),
        createJob({ organization: 'Google' }),
      ]

      const result = store.UNIQUE_ORGANIZATIONS
      expect(result).toEqual(new Set(['Google', 'Amazon']))
    })
  })

  describe('INCLUDE_JOB_BY_ORGANIZATION', () => {
    describe('when no organizations are selected', () => {
      it('returns true', () => {
        const jobsStore = useJobsStore()
        const userStore = useUserStore()
        userStore.selectedOrganizations = []
        const job = createJob({ organization: 'Google' })

        const result = jobsStore.INCLUDE_JOB_BY_ORGANIZATION(job)
        expect(result).toBe(true)
      })
    })

    describe('when organizations are selected', () => {
      it('returns true if job organization is selected', () => {
        const jobsStore = useJobsStore()
        const userStore = useUserStore()
        userStore.selectedOrganizations = ['Google']
        const job = createJob({ organization: 'Google' })

        const result = jobsStore.INCLUDE_JOB_BY_ORGANIZATION(job)
        expect(result).toBe(true)
      })

      it('returns false if job organization is not selected', () => {
        const jobsStore = useJobsStore()
        const userStore = useUserStore()
        userStore.selectedOrganizations = ['Google']
        const job = createJob({ organization: 'Amazon' })

        const result = jobsStore.INCLUDE_JOB_BY_ORGANIZATION(job)
        expect(result).toBe(false)
      })
    })
  })

  describe('INCLUDE_JOB_BY_JOB_TYPE', () => {
    describe('when no job types are selected', () => {
      it('returns true', () => {
        const jobsStore = useJobsStore()
        const userStore = useUserStore()
        userStore.selectedJobTypes = []
        const job = createJob({ jobType: 'Full Time' })

        const result = jobsStore.INCLUDE_JOB_BY_JOB_TYPE(job)
        expect(result).toBe(true)
      })
    })

    describe('when job types are selected', () => {
      it('returns true if job type is selected', () => {
        const jobsStore = useJobsStore()
        const userStore = useUserStore()
        userStore.selectedJobTypes = ['Full Time']
        const job = createJob({ jobType: 'Full Time' })

        const result = jobsStore.INCLUDE_JOB_BY_JOB_TYPE(job)
        expect(result).toBe(true)
      })

      it('returns false if job type is not selected', () => {
        const jobsStore = useJobsStore()
        const userStore = useUserStore()
        userStore.selectedJobTypes = ['Full Time']
        const job = createJob({ jobType: 'Part Time' })

        const result = jobsStore.INCLUDE_JOB_BY_JOB_TYPE(job)
        expect(result).toBe(false)
      })
    })
  })

  describe('FILTERED_JOBS', () => {
    it('identifies jobs associated with given organizations', () => {
      const jobsStore = useJobsStore()

      jobsStore.jobs = [
        createJob({ organization: 'Google' }),
        createJob({ organization: 'Amazon' }),
        createJob({ organization: 'Microsoft' }),
      ]

      const userStore = useUserStore()
      userStore.selectedOrganizations = ['Google', 'Microsoft']

      const result = jobsStore.FILTERED_JOBS

      expect(result).toEqual([
        createJob({ organization: 'Google' }),
        createJob({ organization: 'Microsoft' }),
      ])
    })

    describe('when no organizations are selected', () => {
      it('returns all jobs', () => {
        const jobsStore = useJobsStore()

        jobsStore.jobs = [
          createJob({ organization: 'Google' }),
          createJob({ organization: 'Amazon' }),
          createJob({ organization: 'Microsoft' }),
        ]

        const userStore = useUserStore()
        userStore.selectedOrganizations = []

        const result = jobsStore.FILTERED_JOBS

        expect(result).toEqual([
          createJob({ organization: 'Google' }),
          createJob({ organization: 'Amazon' }),
          createJob({ organization: 'Microsoft' }),
        ])
      })
    })
  })
})
