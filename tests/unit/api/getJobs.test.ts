import type { Mock } from 'vitest'

import axios from 'axios'
vi.mock('axios')
const axiosGetMock = axios.get as Mock

import getJobs from '@/api/getJobs'

describe('getJobs', () => {
  beforeEach(() => {
    axiosGetMock.mockResolvedValue({
      data: [
        {
          id: 1,
          title: 'Software Engineer',
        },
      ],
    })
  })
  it('fetches jobs that candidates can apply to', async () => {
    await getJobs()

    expect(axios.get).toHaveBeenCalledWith(`${import.meta.env.VITE_APP_API_URL}/jobs`)
  })

  it('extracts jobs from the response', async () => {
    const jobs = await getJobs()

    expect(jobs).toEqual([
      {
        id: 1,
        title: 'Software Engineer',
      },
    ])
  })
})
