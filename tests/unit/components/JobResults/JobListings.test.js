import { render, screen } from '@testing-library/vue'
import { RouterLinkStub } from '@vue/test-utils'

import { createTestingPinia } from '@pinia/testing'
import { useRoute } from 'vue-router'
vi.mock('vue-router')

import JobListings from '@/components/JobResults/JobListings.vue'
import { useJobsStore } from '@/stores/jobs'

describe('JobListings', () => {
  const renderJobListings = () => {
    const pinia = createTestingPinia()
    const jobsStore = useJobsStore()
    jobsStore.FILTERED_JOBS = Array(15).fill({})

    render(JobListings, {
      global: {
        plugins: [pinia],
        stubs: {
          RouterLink: RouterLinkStub,
        },
      },
    })

    return { jobsStore }
  }

  it('fetches jobs', async () => {
    useRoute.mockReturnValue({ query: {} })
    const { jobsStore } = renderJobListings()

    expect(jobsStore.FETCH_JOBS).toHaveBeenCalled()
  })

  it('renders maximum of 10 job listings', async () => {
    useRoute.mockReturnValue({ query: { page: '1' } })
    const { jobsStore } = renderJobListings()

    jobsStore.FILTERED_JOBS = Array(15).fill({})

    const jobListings = await screen.findAllByRole('listitem')
    expect(jobListings).toHaveLength(10)
  })

  describe('when there is no query param specifief', () => {
    it('shows page number 1', () => {
      useRoute.mockReturnValue({ query: { page: undefined } })
      renderJobListings()

      expect(screen.getByText('Page 1')).toBeInTheDocument()
    })
  })

  describe('when there is a query param specified', () => {
    it('shows the page number', () => {
      useRoute.mockReturnValue({ query: { page: '2' } })
      renderJobListings()

      expect(screen.getByText('Page 2')).toBeInTheDocument()
    })
  })

  describe('when user is on the first page', () => {
    it('does not show the previous button', async () => {
      useRoute.mockReturnValue({ query: { page: '1' } })
      const { jobsStore } = renderJobListings()
      jobsStore.FILTERED_JOBS = Array(15).fill({})

      await screen.findAllByRole('listitem')

      const previousButton = screen.queryByRole('link', {
        name: /previous/i,
      })

      expect(previousButton).not.toBeInTheDocument()
    })

    it('does show the next button', async () => {
      useRoute.mockReturnValue({ query: { page: '2' } })
      const { jobsStore } = renderJobListings()
      jobsStore.FILTERED_JOBS = Array(30).fill({})

      await screen.findAllByRole('listitem')

      const nextButton = screen.queryByRole('link', {
        name: /next/i,
      })

      expect(nextButton).toBeInTheDocument()
    })
  })

  describe('when user is on the last page', () => {
    it('does not show the next button', async () => {
      useRoute.mockReturnValue({ query: { page: '2' } })
      const { jobsStore } = renderJobListings()
      jobsStore.FILTERED_JOBS = Array(15).fill({})

      await screen.findAllByRole('listitem')

      const nextButton = screen.queryByRole('link', {
        name: /next/i,
      })

      expect(nextButton).not.toBeInTheDocument()
    })

    it('does show the previous button', async () => {
      useRoute.mockReturnValue({ query: { page: '2' } })
      const { jobsStore } = renderJobListings()
      jobsStore.FILTERED_JOBS = Array(15).fill({})

      await screen.findAllByRole('listitem')

      const previousButton = screen.queryByRole('link', {
        name: /previous/i,
      })

      expect(previousButton).toBeInTheDocument()
    })
  })
})
