import { render, screen } from '@testing-library/vue'
import userEvent from '@testing-library/user-event'
import { createTestingPinia } from '@pinia/testing'

import { useRouter } from 'vue-router'
vi.mock('vue-router')

import JobFiltersSidebarCheckboxGroup from '@/components/JobResults/JobFiltersSidebar/JobFiltersSidebarCheckboxGroup.vue'

describe('JobFiltersSidebarCheckboxGroup', () => {
  const createProps = (props = {}) => ({
    title: 'Some title',
    uniqueValues: new Set(['Item 1', 'Item 2']),
    action: vi.fn(),
    ...props,
  })

  const renderJobFiltersSidebarCheckboxGroup = (props) => {
    const pinia = createTestingPinia()

    render(JobFiltersSidebarCheckboxGroup, {
      props: {
        ...props,
      },
      global: {
        plugins: [pinia],
        stubs: ['FontAwesomeIcon'],
      },
    })
  }

  it('renders unique list of values', async () => {
    const props = createProps()
    renderJobFiltersSidebarCheckboxGroup(props)

    const button = screen.getByRole('button', { name: /some title/i })
    await userEvent.click(button)

    const listItems = screen.getAllByRole('listitem')
    const values = listItems.map((item) => item.textContent)

    expect(values).toEqual(['Item 1', 'Item 2'])
  })

  describe('when user clicks a checkbox', () => {
    it('communicates that user has selected checkbox for filtering', async () => {
      useRouter.mockReturnValue({ push: vi.fn() })

      const props = createProps()
      renderJobFiltersSidebarCheckboxGroup(props)

      const button = screen.getByRole('button', { name: /some title/i })
      await userEvent.click(button)

      const aCheckbox = screen.getByRole('checkbox', { name: /item 1/i })
      await userEvent.click(aCheckbox)

      expect(props.action).toHaveBeenCalledWith(['Item 1'])
    })

    it('navigates the user to job results page', async () => {
      const push = vi.fn()
      useRouter.mockReturnValue({ push })

      const props = createProps()
      renderJobFiltersSidebarCheckboxGroup(props)

      const button = screen.getByRole('button', { name: /some title/i })
      await userEvent.click(button)

      const aCheckbox = screen.getByRole('checkbox', { name: /item 1/i })
      await userEvent.click(aCheckbox)

      expect(push).toHaveBeenCalledWith({
        name: 'JobResults',
      })
    })
  })
})
