import { render, screen } from '@testing-library/vue'
import { RouterLinkStub } from '@vue/test-utils'

import JobListing from '@/components/JobResults/JobListing.vue'

describe('JobListing', () => {
  const createJobListingProps = (props) => (
    {
      job: {
        id: 9,
        title: 'Job Title',
        organization: 'Organization Name',
        locations: ['Bratislava'],
        minimumQualifications: ['Brain'],
        ...props
      }
    }
  )

  const renderJobListing = (props) => {
    render(JobListing, {
      global: {
        stubs: {
          RouterLink: RouterLinkStub
        }
      },
      props: {...props}
    })
  }

  it('renders the job title', () => {
    const props = createJobListingProps({ title: 'Test' })
    renderJobListing(props)
    expect(screen.getByText('Test')).toBeInTheDocument()
  })

  it('renders job organization', () => {
    const props = createJobListingProps({ organization: 'RDL' })
    renderJobListing(props)
    expect(screen.getByText('RDL')).toBeInTheDocument()
  })

  it('renders job locations', () => {
    const props = createJobListingProps({ locations: ['Toronto', 'Vancouver'] })
    renderJobListing(props)
    expect(screen.getByText('Toronto')).toBeInTheDocument()
    expect(screen.getByText('Vancouver')).toBeInTheDocument()
  })

  it('renders minimum qualifications', () => {
    const props = createJobListingProps({ minimumQualifications: ['Test 1', 'Test 2'] })
    renderJobListing(props)
    expect(screen.getByText('Test 1')).toBeInTheDocument()
    expect(screen.getByText('Test 2')).toBeInTheDocument()
  })
})