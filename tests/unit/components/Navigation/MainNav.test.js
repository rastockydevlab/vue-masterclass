import { render, screen } from '@testing-library/vue'
import userEvent from '@testing-library/user-event'
import { RouterLinkStub } from '@vue/test-utils'
import { createTestingPinia } from '@pinia/testing'

import { useRoute } from 'vue-router'
vi.mock('vue-router')

import MainNav from '@/components/Navigation/MainNav.vue'
import { useUserStore } from '@/stores/user'

describe('MainNav', () => {
  const renderMainNav = () => {
    const pinia = createTestingPinia()
    useRoute.mockReturnValue({ name: 'Home' })

    render(MainNav, {
      global: {
        plugins: [pinia],
        stubs: {
          FontAwesomeIcon: true,
          RouterLink: RouterLinkStub,
        },
      },
    })
  }

  it('displays company name', () => {
    renderMainNav()
    expect(screen.getByText('RDL Careers')).toBeInTheDocument()
  })

  it('displays menu items for navigation', () => {
    renderMainNav()
    const navMenuItems = screen.getAllByRole('listitem')

    const navMenuTexts = navMenuItems.map((item) => item.textContent)

    expect(navMenuTexts).toEqual(['Teams', 'Location', 'Life at RDL', 'Students', 'Hiring', 'Jobs'])
  })

  describe('when the user signs in', () => {
    it('displays user profile picture', async () => {
      renderMainNav()

      const userStore = useUserStore()

      // Make sure the profile picture is not displayed

      const profileImg = screen.queryByRole('img', {
        name: /user profile picture/i,
      })

      expect(profileImg).not.toBeInTheDocument()

      // Click the sign in button

      const signInButton = screen.getByRole('button', {
        name: /sign in/i,
      })

      userStore.isLoggedIn = true
      await userEvent.click(signInButton)

      // Check if the profile picture is displayed
      const profileImgAfterSignIn = screen.getByRole('img', {
        name: /user profile picture/i,
      })

      expect(profileImgAfterSignIn).toBeInTheDocument()
    })
  })
})
