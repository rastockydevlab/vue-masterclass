import { render, screen } from '@testing-library/vue'
import { createTestingPinia } from '@pinia/testing'

import { useRoute } from 'vue-router'
vi.mock('vue-router')

import SubNav from '@/components/Navigation/SubNav.vue'
import { useJobsStore } from '@/stores/jobs'

describe('SubNav', () => {
  const renderSubNav = () => {
    const pinia = createTestingPinia()
    const jobsStore = useJobsStore(pinia)

    render(SubNav, {
      global: {
        plugins: [pinia],
        stubs: {
          FontAwesomeIcon: true,
        },
      },
      props: {
        jobsCount: 5,
      },
    })

    return { jobsStore }
  }

  describe('when user is on jobs page', () => {
    it('displays job count', async () => {
      useRoute.mockReturnValue({ name: 'JobResults' })

      const { jobsStore } = renderSubNav()

      const numberOfJobs = 16
      jobsStore.FILTERED_JOBS = Array(numberOfJobs).fill({})

      const jobCount = await screen.findByText(numberOfJobs)
      expect(jobCount).toBeInTheDocument()
    })
  })

  describe('when user is not on jobs page', () => {
    it('does not display job count', () => {
      useRoute.mockReturnValue({ name: 'Home' })
      const { jobsStore } = renderSubNav()

      const numberOfJobs = 16
      jobsStore.FILTERED_JOBS_BY_ORGANIZATIONS = Array(numberOfJobs).fill({})

      const jobCount = screen.queryByText(numberOfJobs)
      expect(jobCount).not.toBeInTheDocument()
    })
  })
})
