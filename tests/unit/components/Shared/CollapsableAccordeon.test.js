import { render, screen } from '@testing-library/vue'
import userEvent from '@testing-library/user-event'

import CollapsableAccordeon from '@/components/Shared/CollapsableAccordeon.vue'

describe('CollapsableAccordeon', () => {
  const renderCollapsibleAccordeon = ({ props, slots } = {}) => {
    render(CollapsableAccordeon, {
      global: {
        stubs: {
          FontAwesomeIcon: true,
        },
      },
      props,
      slots,
    })
  }

  it('renders child content', async () => {
    const props = {
      title: 'Category',
    }

    const slots = {
      default: '<h3>My nested child</h3>',
    }

    renderCollapsibleAccordeon({ props, slots })

    expect(screen.queryByText('My nested child')).not.toBeInTheDocument()

    const button = screen.getByRole('button', { name: /category/i })

    await userEvent.click(button)

    expect(screen.getByText('My nested child')).toBeInTheDocument()
  })

  describe('when parent does not provide custom child content', () => {
    it('renders default child content', async () => {
      const props = {
        title: 'Category',
      }

      renderCollapsibleAccordeon({ props })

      const button = screen.getByRole('button', { name: /category/i })

      await userEvent.click(button)

      expect(screen.getByText('No content provided')).toBeInTheDocument()
    })
  })
})
