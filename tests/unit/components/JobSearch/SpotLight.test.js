import { render, screen } from '@testing-library/vue'
import axios from 'axios'

import SpotLight from '@/components/JobSearch/SpotLight.vue'

vi.mock('axios')

describe('SpotLight', () => {
  const mockSpotLightResponse = (spotlight = {}) => {
    axios.get.mockResolvedValue({
      data: [
        {
          id: 1,
          img: 'Some image',
          title: 'Spotlight',
          description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
          ...spotlight,
        },
      ],
    })
  }

  it('provides image to parent component', async () => {
    const spotlight = { img: 'ImageUrlTest' }
    mockSpotLightResponse(spotlight)

    render(SpotLight, {
      slots: {
        default: `<template #default="slotProps">
          <h1>{{ slotProps.img }}</h1>
        </template>`,
      },
    })

    const text = await screen.findByText('ImageUrlTest')

    expect(text).toBeInTheDocument()
  })

  it('provides title to parent component', async () => {
    const spotlight = { title: 'Title Test' }
    mockSpotLightResponse(spotlight)

    render(SpotLight, {
      slots: {
        default: `<template #default="slotProps">
          <h1>{{ slotProps.title }}</h1>
        </template>`,
      },
    })

    const text = await screen.findByText('Title Test')

    expect(text).toBeInTheDocument()
  })

  it('provides description to parent component', async () => {
    const spotlight = { description: 'Description Test' }
    mockSpotLightResponse(spotlight)

    render(SpotLight, {
      slots: {
        default: `<template #default="slotProps">
          <h1>{{ slotProps.description }}</h1>
        </template>`,
      },
    })

    const text = await screen.findByText('Description Test')

    expect(text).toBeInTheDocument()
  })
})
