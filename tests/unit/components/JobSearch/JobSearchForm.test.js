import { render, screen } from '@testing-library/vue'
import userEvent from '@testing-library/user-event'

import { useRouter } from 'vue-router'
vi.mock('vue-router')

import JobSearchForm from '@/components/JobSearch/JobSearchForm.vue'

describe('JobSearchForm', () => {
  it('directs user to job results page with users search params', async () => {
    const push = vi.fn()
    useRouter.mockReturnValue({ push })

    render(JobSearchForm, {
      global: {
        stubs: ['FontAwesomeIcon'],
      },
    })

    const roleInput = screen.getByRole('textbox', {
      name: /role/i,
    })

    await userEvent.type(roleInput, 'developer')

    const locationInput = screen.getByRole('textbox', {
      name: /where/i,
    })

    await userEvent.type(locationInput, 'bratislava')

    const searchButton = screen.getByRole('button', {
      name: /search/i,
    })

    await userEvent.click(searchButton)

    expect(push).toHaveBeenCalledWith({
      name: 'JobResults',
      query: {
        jobTitle: 'developer',
        location: 'bratislava',
      },
    })
  })
})
