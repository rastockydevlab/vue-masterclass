import { nextTick } from 'vue'
import { render, screen } from '@testing-library/vue'

import AnimatedHeadline from '@/components/JobSearch/AnimatedHeadline.vue'

describe('AnimatedHeadline', () => {
  beforeEach(() => {
    vi.useFakeTimers()
  })

  afterEach(() => {
    vi.useRealTimers()
    vi.unstubAllGlobals()
  })

  it('displays introductory action verb', () => {
    render(AnimatedHeadline, {
      data() {
        return {
          action: 'create',
          actions: ['create', 'read', 'update', 'delete'],
        }
      },
    })

    const actionPhrase = screen.getByRole('heading', {
      name: /for everyone/i,
    })

    expect(actionPhrase).toBeInTheDocument()
  })

  it('changes action verb at a consistent interval', () => {
    const mockInterval = vi.fn()
    vi.stubGlobal('setInterval', mockInterval)

    render(AnimatedHeadline, {
      data() {
        return {
          action: 'create',
          actions: ['create', 'read', 'update', 'delete'],
        }
      },
    })

    expect(mockInterval).toHaveBeenCalled()
  })

  it('changes action verb after interval', async () => {
    render(AnimatedHeadline, {
      data() {
        return {
          action: 'create',
          actions: ['create', 'read', 'update', 'delete'],
        }
      },
    })

    vi.advanceTimersToNextTimer()
    await nextTick()

    const actionPhrase = screen.getByRole('heading', {
      name: /for everyone/i,
    })

    expect(actionPhrase).toBeInTheDocument()
  })

  it('removes interval when component disappears', () => {
    const clearInterval = vi.fn()
    vi.stubGlobal('clearInterval', clearInterval)

    const { unmount } = render(AnimatedHeadline, {
      data() {
        return {
          action: 'create',
          actions: ['create', 'read', 'update', 'delete'],
        }
      },
    })

    unmount()

    expect(clearInterval).toHaveBeenCalled()
  })
})
