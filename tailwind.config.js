/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: ['./src/index.css', './src/**/*.{vue,js,ts}'],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Open Sans', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        'brand-gray-1': '#dadce0',
        'brand-gray-2': '#f8f9fa',
        'brand-gray-3': '#80868b',
        'brand-blue-1': '#1967d2',
        'brand-blue-2': '#4285f4',
        'brand-green-1': '#137333',
      },
      boxShadow: {
        blue: '0px 0px 10px 0px rgba(66, 122, 245, 0.5)',
        gray: '0px 1px 3px 0px rgba(60, 64, 67, 0.3)',
      },
    },
  },
  plugins: [],
}
